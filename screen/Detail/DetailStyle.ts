import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    key: {
        marginVertical: 16
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 1
    },
    textInput: {
        width: 260,
        alignSelf: 'center',
        borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 6,
        paddingVertical: 10,
        textAlign: 'center'
    },
    button: {
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        backgroundColor: 'grey',
        borderRadius: 10,
        paddingVertical: 8,
        marginTop: 20,
        width: 160
    }
})

export default styles
