import React from 'react'
import { Text, SafeAreaView } from 'react-native'
import { DataTable } from 'react-native-paper'

import styles from './DetailStyle'

export default function Detail({ route }: { route: any }) {

    return (
        <SafeAreaView style={styles.container}>

            <DataTable style={{ width: '96%' }}>
                <DataTable.Header>
                    <DataTable.Title>Parameter</DataTable.Title>
                    <DataTable.Title>Value</DataTable.Title>
                </DataTable.Header>

                <DataTable.Row>
                    <DataTable.Cell>Name</DataTable.Cell>
                    <DataTable.Cell>{route.params.name}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row style={{ height: 100 }}>
                    <DataTable.Cell>Nasa Jpl Url</DataTable.Cell>
                    <Text style={{ width: 200, marginTop: 30 }}>{route.params.nasa_jpl_url}</Text>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Is Potentially Hazardous</DataTable.Cell>
                    <DataTable.Cell>{route.params.is_potentially_hazardous_asteroid.toString()}</DataTable.Cell>
                </DataTable.Row>

            </DataTable>

        </SafeAreaView>
    )
}
