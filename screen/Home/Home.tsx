import React, { useState } from 'react'
import { View } from 'react-native'
import { Button, TextInput, Snackbar } from 'react-native-paper'

import { REACT_APP_singleUserURL, API_KEY, RANDOM_ID_API } from '../../config/env.json'
import styles from './HomeStyle'

export default function Home({ navigation }: { navigation: any }) {

    const [asteroidId, setAsteroidId] = useState('')
    const [loading, setLoading] = useState(false)
    const [loading2, setLoading2] = useState(false)
    const [visible, setVisible] = useState(false)

    const onDismissSnackBar = () => setVisible(false)

    const handleSubmit = () => {
        setLoading(true)
        fetch(REACT_APP_singleUserURL + asteroidId + '/?api_key=' + API_KEY)
            .then(data => data.json())
            .then(value => {
                navigation.navigate('Detail', { name: value.name, nasa_jpl_url: value.nasa_jpl_url, is_potentially_hazardous_asteroid: value.is_potentially_hazardous_asteroid })
                setLoading(false)
            })
            .catch(err => {
                setVisible(true)
                setLoading(false)
            })
    }

    const handleRandomSubmit = () => {
        setLoading2(true)
        fetch(RANDOM_ID_API + API_KEY)
            .then(data => data.json())
            .then(value => {
                navigation.navigate('Detail', { name: value.near_earth_objects[Math.floor(Math.random() * value.near_earth_objects.length)].name, nasa_jpl_url: value.near_earth_objects[Math.floor(Math.random() * value.near_earth_objects.length)].nasa_jpl_url, is_potentially_hazardous_asteroid: value.near_earth_objects[Math.floor(Math.random() * value.near_earth_objects.length)].is_potentially_hazardous_asteroid })
                setLoading2(false)
            })
    }

    return (
        <View style={styles.container}>

            <TextInput
                label="Enter Asteroid ID"
                value={asteroidId}
                style={styles.textInput}
                onChangeText={(value) => setAsteroidId(value)}
                mode='outlined'
                keyboardType='number-pad'
                activeOutlineColor='grey'
            />

            <Button
                mode="contained"
                onPress={() => handleSubmit()}
                color='grey'
                style={{ marginTop: 20 }}
                labelStyle={{ color: '#FFF' }}
                disabled={asteroidId.trim() === '' ? true : false}
                loading={loading}
            >
                Submit
            </Button>

            <Button
                mode="contained"
                onPress={() => handleRandomSubmit()}
                color='grey'
                style={{ marginTop: 20 }}
                labelStyle={{ color: '#FFF' }}
                loading={loading2}
            >
                Random Asteroid
            </Button>

            <Snackbar
                visible={visible}
                onDismiss={onDismissSnackBar}
                action={{
                    label: 'OK'
                }}>
                Please Enter A Valid ID
            </Snackbar>

        </View>
    )
}
