import { StyleSheet, LogBox } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'

import Home from './screen/Home/Home'
import Detail from './screen/Detail/Detail'

LogBox.ignoreAllLogs(true)

const Stack = createStackNavigator()

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name='Home' component={Home} options={{ title: 'HOME', headerTitleAlign: 'center' }} />
        <Stack.Screen name='Detail' component={Detail} options={{ title: 'DETAILS', headerTitleAlign: 'center' }} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
